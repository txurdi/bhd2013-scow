<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

$app->get('/', function () use ($app) {
	$datosTwig = array();
	$datosTwig['inicio'] = true;


	return $app['twig']->render('index.twig', $datosTwig);
})->bind('inicio');

$app->get('/pistas', function () use ($app) {
    $datosTwig = array();


    return $app['twig']->render('pista.twig', $datosTwig);
})->bind('pistas');


$app->get('/colaborar', function () use ($app) {
    $datosTwig = array();


    return $app['twig']->render('pista.twig', $datosTwig);
})->bind('colaborar');

















































$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) {
        return;
    }
    $page = 404 == $code ? '404.twig' : '500.twig';
    return new Response($app['twig']->render($page, array('code' => $code)), $code);
});

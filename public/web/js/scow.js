/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$( document ).ready(function() {

    SC.initialize({
        client_id: "c14b48829541a9a79a279bf1605171ac",
        redirect_uri: "http://bime.txurdi.net"
    });
    SC.get('/me', function(me) {
        if (me.errors != null) {
            desconectadoUsuario();
        } else {
            conectadoUsuario();
        }
    });

    var SCOW = {};

    //SCOW.Tracks = ["121091305","121091308","121091306","121091307"];
    SCOW.Tracks = ["121185714","121185719","121185713"];
    SCOW.Datos = [];
    SCOW.Sounds = [];
    SCOW.Base = "121185716";//"121182339";
    SCOW.DatosBase = "";
    SCOW.SoundBase = "";

    $("#precarga").on("click", function() {
        cargaListaDatos(SCOW.Tracks);
        aniadeColabora();
        cargaListaSounds(SCOW.Tracks);
    })
    $("#stream").on("click", function(){
        playBase(SCOW.Base);
        playListaSounds(SCOW.Tracks);
    });


    $("#conecta").on("click", function(){
        SC.connect(function() {
            conectadoUsuario();
        });
    });
    $("#litaMisTracks").on("click", function(){
        SC.connect(function() {
            SC.get('/me', function(me) {
                alert('Hello, ' + me.username);
            });
        });
    });


    $("#cargabase").on("click", function(){
        cargaBase(SCOW.Base);
    });
    $("#playbase").on("click", function(){
        playBase(SCOW.Base);
    });
    $("#pausebase").on("click", function(){
        pauseBase(SCOW.Base);
    });

    $("#colabora").on("click", function(){
        SC.connect(function() {
            SC.get('/me', function(me) {
                alert('HOLA '+me.username+',\nSe supone que ahora te listaría tus tracks para que elijas el que quieras.\nLo siento, NO HA HABIDO TIEMPO PARA MÁS');
            });
            conectadoUsuario();
        });
    });



    function conectadoUsuario() {
        SC.get('/me', function(me) {
            $("#usuario").text(me.username);
            $("#conecta").hide();
            $("#desconecta").show();
            //alert('Hello, ' + me.username);
        });

    }

    function desconectadoUsuario() {
        $("#usuario").text("anonimo");
        $("#conecta").show();
        $("#desconecta").hide();

    }

    function dibujaBase() {
 //       debugger;
        $("#base .wave").html("<img src=\""+SCOW.DatosBase.waveform_url+"\" height=\"40px\" width=\"100%\" />");
        $("#base h3").html("<a href=\""+SCOW.DatosBase.permalink_url+"\">"+SCOW.DatosBase.title+"</a>");
        $("#base p.desc").html(SCOW.DatosBase.description);
        $("#base p.autor").html(SCOW.DatosBase.user.username);
    }

    function dibujaLog(texto) {
        var $myNewElement = $( "<li>"+texto+"</li>" );
        //$myNewElement.appendTo( "ul#log" );
        $( "ul#log li:first").before($myNewElement);

    }

    function aniadePista(track) {
        var $myNewElement = $(
            "<div class=\"col-md-2\" id=\"tr_"+track+"\">"+
                "    <div class=\"thumbnail\">"+
                "        <div class=\"caption\">"+
                "            <div class='wave'> <img src=\""+SCOW.Datos[track].waveform_url+"\" height=\"40px\" width=\"100%\" /></div>"+
                "            <h3><a href=\""+SCOW.Datos[track].permalink_url+"\">"+SCOW.Datos[track].title+"</a></h3>"+
                "            <p>"+SCOW.Datos[track].description+"</p>"+
                "            <p>Autor: "+SCOW.Datos[track].user.username+"</p>"+
                "        </div>"+
                "        <div class=\"estado\">ESTADO"+
                "        </div>"+
                "    </div>"+
                "</div>"
        );
        $myNewElement.appendTo( "#trocitos" );
    }

    function aniadeColabora() {
        $("#tr_nuevo").show();
/*        var $myNewElement = $(
            "<div class=\"col-md-2\" id=\"tr_nuevo\">"+
                "    <div class=\"thumbnail\">"+
                "        <div class=\"caption\">"+
                "            <h3>¡COLABORA!</h3>"+
                "            <p><a href=\"#\" id=\"colabora\" >Añade un track</a></p>"+
                "        </div>"+
                "    </div>"+
                "</div>"
        );
        $myNewElement.appendTo( "#trocitos" );
        $( "#trocitos").append($myNewElement);*/
    }

    function inicia(track) {
        dibujaLog("Empieza "+ SCOW.Datos[track].title);
        $("#escuchando_ahora").text(SCOW.Datos[track].title);
        $("#tr_"+track+" .estado").text("PLAY");
        $("#tr_"+track).addClass("sonando");
    }
    function acaba(track) {
        dibujaLog("Acaba "+ SCOW.Datos[track].title);
        $("#escuchando_ahora").text("-");
        $("#tr_"+track+" .estado").text("PLAYED");
        $("#tr_"+track).addClass("sonado");
    }
    function cargaSound(track) {
        dibujaLog( "PreLoad "+ SCOW.Datos[track].title);
        $("#tr_"+track+" .estado").text("DATOS");
        $("#tr_"+track).addClass("precargada");
    }
    function cargaDato(track) {
        dibujaLog( "Datos de "+ SCOW.Datos[track].title);
        aniadePista(track);
        $("#tr_"+track+" .estado").text("LOADED");
        $("#tr_"+track).addClass("predatos");
    }



    function cargaListaDatos(lista) {
        var track = lista[0];
        var restoLista = lista.slice(1);
        SC.get("/tracks/"+track, function(objTrack){
            SCOW.Datos[track] = objTrack;
            cargaDato(track);
        });
        if (restoLista.length > 0) {
            cargaListaDatos(restoLista)
        }
    }

    function cargaListaSounds(lista) {
        var track = lista[0];
        var restoLista = lista.slice(1);

        SC.stream("/tracks/"+track, function(sound){
            sound.load();
            SCOW.Sounds[track] = sound;
            cargaSound(track);
            if (restoLista.length > 0) {
                cargaListaSounds(restoLista)
            }
        });
    }


    function playListaSounds(lista) {
        var track = lista[0];
        var sonido = SCOW.Sounds[track];
        var restoLista = lista.slice(1);

        sonido.setPosition(1000);
        sonido.play();
        inicia(track);
        window.setTimeout(function(){
            sonido.pause();
            acaba(track);
            if (restoLista.length > 0) {
                playListaSounds(restoLista)
            }
        }, 4000);
    };


    function playBase() {
        SCOW.SoundBase.play();
    }

    function pauseBase() {
        SCOW.SoundBase.pause();
    }

    function cargaBase(track) {
        SC.stream("/tracks/"+track, function(sound){
            sound.load();
            SC.get("/tracks/"+track, function(objTrack){
//                debugger;
                SCOW.DatosBase = objTrack;
                dibujaBase();
            });
            SCOW.SoundBase = sound;
        });
    }


    /*
     function playLista(lista) {
     restoLista = lista.slice(1);
     SC.stream("/tracks/"+lista[0], function(sound){
     sound.play();
     inicia(lista[0]);
     window.setTimeout(function(){
     sound.pause();
     acaba(lista[0]);
     if (restoLista.length > 0) {
     playLista(restoLista)
     }
     }, 5000);
     });
     }*/
/*
    SC.initialize({
      client_id: "bed275b77c13c160ae997fe81c447b52",
      redirect_uri: "http://connect.soundcloud.com/examples/callback.html"
    });

    $("#recorderUI.reset #controlButton").on("click", function(e){
       updateTimer(0);
       SC.record({
            start: function(){
                setRecorderUIState("recording");
            },
            progress: function(ms, avgPeak){
                updateTimer(ms);
            }
        });
        e.preventDefault();
    });

    $("#recorderUI.recording #controlButton, #recorderUI.playing #controlButton").on("click", function(e){
        setRecorderUIState("recorded");
        SC.recordStop();
        e.preventDefault();
    });

    $("#recorderUI.recorded #controlButton").on("click", function(e){
        updateTimer(0);
        setRecorderUIState("playing");
        SC.recordPlay({
            progress: function(ms){
                updateTimer(ms);
            },
            finished: function(){
                setRecorderUIState("recorded");
            }
        });
        e.preventDefault();
    });

    $("#reset").on("click", function(e){
        SC.recordStop();
        setRecorderUIState("reset");
        e.preventDefault();
    });

    $("#upload").on("click", function(e){
        setRecorderUIState("uploading");

        SC.connect({
            connected: function(){
                $("#uploadStatus").html("Uploading...");
                SC.recordUpload({
                    track: {
                        title: "Untitled Recording",
                        sharing: "private"
                    }
                }, function(track){
                    $("#uploadStatus").html("Uploaded: <a href='" + track.permalink_url + "'>" + track.permalink_url + "</a>");
                });
            }
        });

        e.preventDefault();
    });

    function updateTimer(ms){
        $("#timer").text(SC.Helper.millisecondsToHMS(ms));
    }

    function setRecorderUIState(state){
        // state can be reset, recording, recorded, playing, uploading
        // visibility of buttons is managed via CSS
        $("#recorderUI").attr("class", state);
    }
*/









});

<?php
use Symfony\Component\ClassLoader\DebugClassLoader;
use Symfony\Component\HttpKernel\Debug\ErrorHandler;
use Symfony\Component\HttpKernel\Debug\ExceptionHandler;

ini_set('display_errors', 0);
setlocale(LC_ALL, 'es_ES');
setlocale(LC_TIME, 'es_ES.UTF-8');
date_default_timezone_set('CET');

if (apache_getenv('SOCSERV')==="local") {
    //if (($_SERVER["SERVER_NAME"]=="app.aplisoc.com") || ($_GET["txurdi"]=="si")) {
	//var_dump('ZONA DEV');exit();
	include 'index_dev.php';
} else {
	//echo "ESTAMOS DESARROLLANDO TODAVÍA... DANOS TIEMPO...";
	//exit();
	//var_dump('ZONA PROD');exit();
	require_once __DIR__.'/../vendor/autoload.php';

/*error_reporting(-1);
DebugClassLoader::enable();
ErrorHandler::register();
if ('cli' !== php_sapi_name()) {
    ExceptionHandler::register();
}*/

	$app = require __DIR__.'/../src/app.php';
	require __DIR__.'/../config/prod.php';
	require __DIR__.'/../src/controllers.php';
	$app->run();	
	
 }
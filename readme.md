# SCOW - Social COlaboration Wave

20-21 de Noviembre de 2013
BIME Hack Day Bilbao

Usando la API de SoundCloud, la idea era componer una canción colaborativamente, añadiendo cada uno 4 segundos.

En el Hack nos ha dado tiempo a hacer unas pruebas de cargar diferentes tracks guardados en SoundCloud, y jugar con play, pause, y precargas para poder hacer sonar una série de tracks una detrás de otra sobre una base elegida.

Al final, se le da la opción al usuario a que se loguee y añada un track para continuar la canción. (y hasta aquí se puede leer)

## Autor: 
 - @txurdi
## Colaboraron: 
 - Alex
 - @eifersucht
 - @alonso_usa




